package com.chetjlemon.redditreader03;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.chetjlemon.redditreader02.R;

public class MainActivity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    void addFragment(){
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragments_holder
                        , PostsFragment.newInstance("pics"))
                .commit();
    }

    public final static String POST_URL = "com.chetjlemon.redditreader02.POST_URL";
    public void viewLink(View view){
        Intent intent = new Intent(view.getContext(), LinkView.class);
        String postUrl = (String)((TextView)view).getText();
        System.out.println("url?? "+postUrl);
        intent.putExtra(POST_URL, postUrl);
        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.log_in:
                logInPopUp();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    PopupWindow pw;
    Button cancelButton;
    Button loginButton;

    public void logInPopUp(){
        LayoutInflater inflater = (LayoutInflater)
                this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.login_popup,
                (ViewGroup) findViewById(R.id.popup_window));

        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();  // deprecated
        int height = display.getHeight();  // deprecated

        pw = new PopupWindow(layout, width-50, 600, true);
        pw.showAtLocation(layout, Gravity.CENTER, 0, 0);

        cancelButton = (Button)layout.findViewById(R.id.cancel_button);
        loginButton = (Button)layout.findViewById(R.id.login_button);
        cancelButton.setOnClickListener(cancel_button_click_listener);
        loginButton.setOnClickListener(login_button_click_listener);
    }

    private OnClickListener cancel_button_click_listener = new OnClickListener() {
        public void onClick(View v) {
            pw.dismiss();

        }
    };

    private OnClickListener login_button_click_listener = new OnClickListener() {
        public void onClick(View v) {
            Login loginTry = new Login();

            EditText userField = (EditText)findViewById(R.id.username);
            EditText pwField = (EditText)findViewById(R.id.password);

            boolean itsNull = false;
            boolean loginResult = false;
            String userEntry = "";
            String pwEntry = "";

            try{
                userEntry = userField.getText().toString();
                pwEntry = pwField.getText().toString();
            }catch (NullPointerException e){
                itsNull = true;
            }

            if(!itsNull)
                loginResult = loginTry.login(userEntry, pwEntry);
            if(loginResult)
                pw.dismiss();
            else{
                TextView userpwError = (TextView)findViewById(R.id.invalid_login_text);

                userpwError.setAlpha(1);
            }


        }
    };


}


