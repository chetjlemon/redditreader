package com.chetjlemon.redditreader03;

/**
 * Created by Chet on 6/26/13.
 *
 * This is a class that holds the data of the JSON objects
 * returned by the Reddit API
 */
public class Post{

    String subreddit;
    String title;
    String author;
    int points;
    int numComments;
    String permalink;
    String url;
    String domain;
    String id;
    String thumbnail;

    String getDetails(){
        String details=author
                +" - "
                +numComments
                +" comments";

        return details;
    }

    String getThumbnail(){
        return thumbnail;
    }

    String getId(){
        return id;
    }

    String getUrl(){
        return url;
    }

    String getScore(){
        return Integer.toString(points);
    }
}