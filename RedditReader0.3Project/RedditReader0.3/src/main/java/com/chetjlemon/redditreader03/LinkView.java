package com.chetjlemon.redditreader03;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.chetjlemon.redditreader02.R;

/**
 * Created by Chet on 6/26/13.
 *
 * opens the url in a webview
 */
public class LinkView extends Activity {

    WebView webview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // fullscreen mode (title bar and notif bar invisible)
        /*requestWindowFeature(Window.FEATURE_NO_TITLE);                      // removes title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  // removes notif bar
                              WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.link_view);*/

        // title bar and notif bar visible
        /*requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.linkview_titlebar);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.linkview_titlebar);*/


        // Get the post url from the intent
        Intent intent = getIntent();
        String postUrl = intent.getStringExtra(MainActivity.POST_URL);

        webview = new WebView(this);
        setContentView(webview);
        // Simplest usage: note that an exception will NOT be thrown
        // if there is an error loading this page (see below).
        webview.setInitialScale(30);
        WebSettings webSettings = webview.getSettings();
        webSettings.setUseWideViewPort(true);
        webview.setWebViewClient(new WebViewClient());

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        // if apilevel >= 11 (honeycomb)
        if (currentapiVersion >= 11){
            webview.getSettings().setBuiltInZoomControls(true);
            webview.getSettings().setDisplayZoomControls(false); // dis shiet doesnt work if api<11
        } else{
            webview.getSettings().setBuiltInZoomControls(false);
        }

        webview.loadUrl(postUrl);
    }

    public void backClick(View view){
        if(webview.canGoBack())
            webview.goBack();
    }

    public void forwardClick(View view){
        if(webview.canGoForward())
            webview.goForward();
    }


    // hide and show title bar. idk how to button so not used right now
    public void viewTitleBar(View view){
        // title bar and notif bar visible
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.linkview_titlebar);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.linkview_titlebar);
    }

}